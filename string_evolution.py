#Evolving a string from a random set of characters to the target string
import string
import random
#Sys module is used for parsing command line arguments
import sys

def generate_population():
    global target
    for c in range(population):
        #Creating a DNA with random characters 
        dna = ''.join(random.choices(string.ascii_lowercase + string.ascii_uppercase, k = len(target)))
        generation.append(dna)
        
#Calculates fitness of DNA by calculating the closeness of the dna with the target string
def fit_test(dna, target):
    fit_value = 0
    for i in range(len(dna)):
        fit_value += (ord(dna[i]) - ord(target[i])) **2
    return fit_value

#Finds the best DNA by comparing the fitness value of each DNA
def find_best_dna():
    global target
    best_dna = ''
    best_fit_value = fit_test(generation[0], target)
    for d in generation:
        f = fit_test(d, target)
        if (f) < best_fit_value:
            best_dna = d
            best_fit_value = f
    return best_dna

#Mutating DNA by randomly changing the characters at any two positions
def mutate(dna):
    for p in range(population):
        for pos in random.choices(range(len(dna)), k=2):
            mutated_dna = list(dna)
            mutated_dna[pos] = chr(ord(dna[pos]) + random.randint(-1,1))
        mutated_dna = ''.join(mutated_dna)
        generation.append(mutated_dna)
        #print(generation)
    return generation

if __name__ == '__main__':
    #Collecting the target string passed as command line argument
    target = sys.argv[1]
    population = 20
    #g is a generation counter
    g = 1
    #An array to store the entire population (collection of strings) of a generation
    generation = []
    generate_population()   
    while True:
        #Printing generation count
        print("Generation : ", str(g))
        #Printing the best string(DNA) of the generation
        best_dna = find_best_dna()
        print("Best DNA : ", best_dna)
        generation = mutate(best_dna)
        #Checking if the target DNA is achieved
        if(best_dna == target):
            print ("Target DNA attained at generation - ", g)
            break
        g = g + 1
        
        


